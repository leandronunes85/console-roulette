package com.leandronunes85.gamesys.consoleroulette;

import com.leandronunes85.gamesys.consoleroulette.datasource.UsersOnFile;
import com.leandronunes85.gamesys.consoleroulette.engine.ConsoleRouletteEngine;

import java.time.Duration;

/**
 * Created by nunesl on 30/11/15.
 */
public class Launcher {


    public static void main(String[] args) {

        UsersOnFile usersOnFile = new UsersOnFile("src/main/resources/users.txt");

        new ConsoleRouletteEngine(usersOnFile, Duration.ofSeconds(30));


    }

}
