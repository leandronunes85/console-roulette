package com.leandronunes85.gamesys.consoleroulette.model;

import java.util.Objects;

/**
 * Created by nunesl on 30/11/15.
 */
public class User {

    private final String name;
    private double totalWin;
    private double totalBet;

    public User(String name) {
        this(name, 0, 0);
    }
    public User(String name, double totalWin, double totalBet) {
        this.name = name;
        this.totalWin = totalWin;
        this.totalBet = totalBet;
    }

    public String getName() {
        return name;
    }

    public double getTotalWin() {
        return totalWin;
    }

    public double getTotalBet() {
        return totalBet;
    }

    public void addBet(double howMuch) {
        totalBet += howMuch;
    }
    public void addWinnings(double howMuch) {
        totalWin += howMuch;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return Objects.equals(name, user.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name);
    }
}
