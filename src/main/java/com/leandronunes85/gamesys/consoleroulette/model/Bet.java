package com.leandronunes85.gamesys.consoleroulette.model;

/**
 * Created by nunesl on 30/11/15.
 */
public class Bet {

    private final User user;
    private final String runner;
    private final double stake;

    public Bet(User user, String runner, double stake) {
        this.user = user;
        this.runner = runner;
        this.stake = stake;
    }

    public User getUser() {
        return user;
    }

    public String getRunner() {
        return runner;
    }

    public double getStake() {
        return stake;
    }
}
