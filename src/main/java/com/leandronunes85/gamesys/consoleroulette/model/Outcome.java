package com.leandronunes85.gamesys.consoleroulette.model;

/**
 * Created by nunesl on 30/11/15.
 */
public enum Outcome {
    WON,
    LOST
}
