package com.leandronunes85.gamesys.consoleroulette.engine;

import com.leandronunes85.gamesys.consoleroulette.engine.result.Result;
import com.leandronunes85.gamesys.consoleroulette.engine.result.ResultFactory;
import com.leandronunes85.gamesys.consoleroulette.model.Outcome;

import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * Created by nunesl on 30/11/15.
 */
public class OutcomeCalculatorImpl implements OutcomeCalculator {

    private final Set<Integer> winningNumbers;
    private final double winningFactor;
    private final ResultFactory resultFactory;

    public OutcomeCalculatorImpl(int winningNumber, double winningFactor) {
        this(IntStream.of(winningNumber), winningFactor);
    }

    public OutcomeCalculatorImpl(IntStream winningNumbers, double winningFactor) {
        this.winningNumbers = winningNumbers.boxed().collect(Collectors.toSet());
        this.winningFactor = winningFactor;
        this.resultFactory = new ResultFactory();
    }

    @Override
    public Result computeResult(int drawnValue) {
        Outcome outcome = winningNumbers.contains(drawnValue) ? Outcome.WON : Outcome.LOST;
        return resultFactory.getResult(outcome, winningFactor);
    }
}
