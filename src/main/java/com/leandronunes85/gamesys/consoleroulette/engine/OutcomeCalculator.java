package com.leandronunes85.gamesys.consoleroulette.engine;

import com.leandronunes85.gamesys.consoleroulette.engine.result.LostResult;
import com.leandronunes85.gamesys.consoleroulette.engine.result.Result;

/**
 * Created by nunesl on 30/11/15.
 */
public interface OutcomeCalculator {

    OutcomeCalculator NO_OUTCOME_CALCULATOR = (drawnValue) -> LostResult.INSTANCE;

    Result computeResult(int drawnValue);

}
