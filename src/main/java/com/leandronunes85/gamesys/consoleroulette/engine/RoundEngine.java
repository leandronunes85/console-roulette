package com.leandronunes85.gamesys.consoleroulette.engine;

import com.leandronunes85.gamesys.consoleroulette.engine.result.Result;
import com.leandronunes85.gamesys.consoleroulette.input.ConsoleParser;
import com.leandronunes85.gamesys.consoleroulette.input.Parser;
import com.leandronunes85.gamesys.consoleroulette.model.Bet;
import com.leandronunes85.gamesys.consoleroulette.model.User;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static com.leandronunes85.gamesys.consoleroulette.engine.OutcomeCalculator.NO_OUTCOME_CALCULATOR;
import static java.lang.String.format;

/**
 * Created by nunesl on 01/12/15.
 */
public class RoundEngine {

    private static final Parser LINE_PARSER = new ConsoleParser();

    private final Map<String, User> users;
    private final Map<String, OutcomeCalculator> outcomeCalculatorForRunner;
    private final List<Bet> betsForRound;

    public RoundEngine(Map<String, User> users, Map<String, OutcomeCalculator> outcomeCalculatorForRunner) {
        this.users = users;
        this.outcomeCalculatorForRunner = outcomeCalculatorForRunner;
        this.betsForRound = new ArrayList<>();
    }

    public void newLine(String line) {

        Parser.Input input = LINE_PARSER.parse(line);

        User user = users.get(input.getUsername());
        if (user != null) {
            Bet bet = new Bet(user, input.getRunner(), input.getStake());
            synchronized (betsForRound) {
                betsForRound.add(bet);
            }
            user.addBet(input.getStake());
        }
    }

    public void endRound(int drawnNumber) {
        System.out.println("Number: " + drawnNumber);
        System.out.println("Player\tBet\tOutcome\tWinnings");
        System.out.println("---");
        betsForRound.forEach(bet -> {
            OutcomeCalculator outcomeCalculator = outcomeCalculatorForRunner.getOrDefault(bet.getRunner(), NO_OUTCOME_CALCULATOR);
            Result result = outcomeCalculator.computeResult(drawnNumber);
            double winnings = result.computeWinnings(bet.getStake());

            bet.getUser().addWinnings(winnings);
            System.out.println(format("%s\t%s\t%s\t%.1f",
                    bet.getUser().getName(), bet.getRunner(), result.getOutcome(), winnings));
        });
        System.out.println();
        System.out.println();
        System.out.println();
        System.out.println("Player\tTotal Win\tTotal Bet");
        System.out.println("---");
        users.values().forEach(u -> System.out.println(format("%s\t%.1f\t%.1f", u.getName(), u.getTotalWin(), u.getTotalBet())));
    }
}
