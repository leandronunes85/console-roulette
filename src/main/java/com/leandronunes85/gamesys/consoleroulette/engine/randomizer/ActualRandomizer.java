package com.leandronunes85.gamesys.consoleroulette.engine.randomizer;

import java.security.SecureRandom;
import java.util.Random;

/**
 * Created by nunesl on 01/12/15.
 */
public class ActualRandomizer implements RouletteRandomizer {

    private static final Random RANDOM = new SecureRandom();

    private final int maxInclusive;

    public ActualRandomizer(int maxInclusive) {
        this.maxInclusive = maxInclusive;
    }


    @Override
    public int next() {
        return RANDOM.nextInt(maxInclusive - 1) + 1;
    }
}
