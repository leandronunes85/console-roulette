package com.leandronunes85.gamesys.consoleroulette.engine.result;

import com.leandronunes85.gamesys.consoleroulette.model.Outcome;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by nunesl on 01/12/15.
 */
public class ResultFactory {

    private final Map<Double, Result> cache = new HashMap<>();

    public Result getResult(Outcome outcome, double factor) {

        return outcome == Outcome.LOST ?
                LostResult.INSTANCE :
                cache.computeIfAbsent(factor, (f) -> new WonResult(f));
    }

}
