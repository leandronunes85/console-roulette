package com.leandronunes85.gamesys.consoleroulette.engine;

import com.leandronunes85.gamesys.consoleroulette.datasource.UserDataSource;
import com.leandronunes85.gamesys.consoleroulette.engine.randomizer.ActualRandomizer;
import com.leandronunes85.gamesys.consoleroulette.engine.randomizer.RouletteRandomizer;
import com.leandronunes85.gamesys.consoleroulette.model.User;

import java.time.Duration;
import java.util.Map;
import java.util.Scanner;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicReference;
import java.util.function.Function;
import java.util.function.IntPredicate;

import static java.util.stream.Collectors.toMap;
import static java.util.stream.IntStream.rangeClosed;

/**
 * Created by nunesl on 30/11/15.
 */
public class ConsoleRouletteEngine {

    private static final int ROULETTE_MAX = 36;
    private static final IntPredicate EVENS = i -> i % 2 == 0;
    private static final IntPredicate ODDS = EVENS.negate();

    private final Map<String, User> users;
    private final Map<String, OutcomeCalculator> outcomeCalculatorForRunner;
    private final AtomicReference<RoundEngine> currentRound;

    public ConsoleRouletteEngine(UserDataSource userDataSource,
                                 Duration gameDuration) {
        this(userDataSource, gameDuration, new ActualRandomizer(ROULETTE_MAX), new Scanner(System.in));
    }

    ConsoleRouletteEngine(UserDataSource userDataSource,
                          Duration gameDuration,
                          RouletteRandomizer randomizer,
                          Scanner scanner) {

        this.users = userDataSource.loadAllUsers().stream().collect(toMap(User::getName, Function.identity()));

        this.outcomeCalculatorForRunner = rangeClosed(1, ROULETTE_MAX).boxed().collect(toMap(String::valueOf, i -> new OutcomeCalculatorImpl(i, 36.0)));
        this.outcomeCalculatorForRunner.put("EVEN", new OutcomeCalculatorImpl(rangeClosed(1, ROULETTE_MAX).filter(EVENS), 2.0));
        this.outcomeCalculatorForRunner.put("ODD", new OutcomeCalculatorImpl(rangeClosed(1, ROULETTE_MAX).filter(ODDS), 2.0));

        this.currentRound = new AtomicReference<>(new RoundEngine(users, outcomeCalculatorForRunner));

        Executors.newSingleThreadScheduledExecutor().scheduleAtFixedRate(() -> {

            RoundEngine previousRound = this.currentRound.getAndSet(new RoundEngine(users, outcomeCalculatorForRunner));
            int drawn = randomizer.next();
            previousRound.endRound(drawn);

        }, gameDuration.getSeconds(), gameDuration.getSeconds(), TimeUnit.SECONDS);


        new Thread(() -> {
            while (true) {
                String line = scanner.nextLine();
                this.currentRound.get().newLine(line);
            }
        }).start();
    }
}
