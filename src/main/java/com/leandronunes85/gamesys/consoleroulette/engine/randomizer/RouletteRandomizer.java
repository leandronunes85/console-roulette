package com.leandronunes85.gamesys.consoleroulette.engine.randomizer;

/**
 * Created by nunesl on 01/12/15.
 */
public interface RouletteRandomizer {
    int next();
}
