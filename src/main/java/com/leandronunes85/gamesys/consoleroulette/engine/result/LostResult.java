package com.leandronunes85.gamesys.consoleroulette.engine.result;

import com.leandronunes85.gamesys.consoleroulette.model.Outcome;

/**
 * Created by nunesl on 01/12/15.
 */
public class LostResult implements Result {

    public static final Result INSTANCE = new LostResult();

    private LostResult() {}

    @Override
    public Outcome getOutcome() {
        return Outcome.LOST;
    }

    @Override
    public double computeWinnings(double stake) {
        return 0;
    }
}
