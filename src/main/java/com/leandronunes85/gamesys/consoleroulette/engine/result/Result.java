package com.leandronunes85.gamesys.consoleroulette.engine.result;

import com.leandronunes85.gamesys.consoleroulette.model.Outcome;

/**
 * Created by nunesl on 01/12/15.
 */
public interface Result {
    Outcome getOutcome();

    double computeWinnings(double stake);
}
