package com.leandronunes85.gamesys.consoleroulette.engine.result;

import com.leandronunes85.gamesys.consoleroulette.model.Outcome;

/**
 * Created by nunesl on 01/12/15.
 */
public class WonResult implements Result {

    private final double factor;

    WonResult(double factor) {
        this.factor = factor;
    }

    @Override
    public Outcome getOutcome() {
        return Outcome.WON;
    }

    @Override
    public double computeWinnings(double stake) {
        return factor * stake;
    }
}
