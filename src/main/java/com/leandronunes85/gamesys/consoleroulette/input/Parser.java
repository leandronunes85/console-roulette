package com.leandronunes85.gamesys.consoleroulette.input;

/**
 * Created by nunesl on 01/12/15.
 */
public interface Parser {


    Input parse(String line);

    class Input {
        private final String username;
        private final String runner;
        private final double stake;

        public Input(String username, String runner, double stake) {
            this.username = username;
            this.runner = runner;
            this.stake = stake;
        }

        public String getUsername() {
            return username;
        }

        public String getRunner() {
            return runner;
        }

        public double getStake() {
            return stake;
        }
    }
}
