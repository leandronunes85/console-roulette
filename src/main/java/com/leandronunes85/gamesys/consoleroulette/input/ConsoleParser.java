package com.leandronunes85.gamesys.consoleroulette.input;

/**
 * Created by nunesl on 01/12/15.
 */
public class ConsoleParser implements Parser {

    @Override
    public Input parse(String line) {
        String[] parts = line.split(" ");

        String username = parts[0];
        String runner = parts[1];
        double stake = Double.parseDouble(parts[2]);

        return new Input(username, runner, stake);
    }
}
