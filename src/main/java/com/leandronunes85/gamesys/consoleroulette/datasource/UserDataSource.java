package com.leandronunes85.gamesys.consoleroulette.datasource;

import com.leandronunes85.gamesys.consoleroulette.model.User;

import java.util.Set;

/**
 * Created by nunesl on 30/11/15.
 */
public interface UserDataSource {
    Set<User> loadAllUsers();

    class UserDataSourceException extends RuntimeException {
        public UserDataSourceException(Throwable e) {
            super(e);
        }
    }
}
