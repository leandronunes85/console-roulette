package com.leandronunes85.gamesys.consoleroulette.datasource;

import com.leandronunes85.gamesys.consoleroulette.model.User;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Created by nunesl on 30/11/15.
 */
public class UsersOnFile implements UserDataSource {
    private static final String SPLITTER = ",";

    private final String filename;

    public UsersOnFile(String filename) {
        this.filename = filename;
    }

    @Override
    public Set<User> loadAllUsers() {
        try {
            return Files.lines(Paths.get(filename)).map(str -> {
                String[] parts = str.split(SPLITTER);
                return parts.length == 3 ?
                        new User(parts[0], Double.parseDouble(parts[1]), Double.parseDouble(parts[2])) :
                        new User(parts[0]);
            }).collect(Collectors.toSet());
        } catch (IOException e) {
            throw new UserDataSourceException(e);
        }
    }
}
