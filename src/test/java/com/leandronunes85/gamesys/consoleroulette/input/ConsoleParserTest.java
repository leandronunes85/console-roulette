package com.leandronunes85.gamesys.consoleroulette.input;

import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

/**
 * Created by nunesl on 01/12/15.
 */
public class ConsoleParserTest {

    private ConsoleParser victim;

    @BeforeMethod
    public void setUp() {
        victim = new ConsoleParser();
    }

    @Test
    public void shouldParseLineWithCorrectFormat() {
        Parser.Input actual = victim.parse("Username Runner 10.0");

        Assert.assertEquals(actual.getUsername(), "Username");
        Assert.assertEquals(actual.getRunner(), "Runner");
        Assert.assertEquals(actual.getStake(), 10.0);
    }
}