package com.leandronunes85.gamesys.consoleroulette.datasource;

import com.leandronunes85.gamesys.consoleroulette.model.User;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.Set;

/**
 * Created by nunesl on 01/12/15.
 */
public class UsersOnFileTest {

    private UsersOnFile victim;

    @BeforeMethod
    public void setUp() {
        victim = new UsersOnFile("src/test/resources/testUsers.txt");
    }

    @Test
    public void shouldReadAllLinesInFile() {
        Set<User> actual = victim.loadAllUsers();
        Assert.assertEquals(actual.size(), 2);
    }
}