package com.leandronunes85.gamesys.consoleroulette.engine.result;

import com.leandronunes85.gamesys.consoleroulette.model.Outcome;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

/**
 * Created by nunesl on 01/12/15.
 */
public class ResultFactoryTest {

    private ResultFactory victim;

    @BeforeMethod
    public void setUp() {
        victim = new ResultFactory();
    }

    @Test
    public void shouldAlwaysSameInstanceForLostOutcomes() {
        Result result1 = victim.getResult(Outcome.LOST, 1.0);
        Result result2 = victim.getResult(Outcome.LOST, 2.0);

        Assert.assertSame(result1, result2);
    }

    @Test
    public void shouldReturnExpectedWinnings() {
        double stake = 2.0;
        double factor = 5.0;

        Result result = victim.getResult(Outcome.WON, factor);
        double actual = result.computeWinnings(stake);

        Assert.assertEquals(actual, factor * stake);
        Assert.assertEquals(result.getOutcome(), Outcome.WON);
    }

    @Test
    public void shouldCacheWonResultsForSameFactor() {
        double factor = 5.0;

        Result result1 = victim.getResult(Outcome.WON, factor);
        Result result2 = victim.getResult(Outcome.WON, factor);

        Assert.assertSame(result1, result2);
    }
}