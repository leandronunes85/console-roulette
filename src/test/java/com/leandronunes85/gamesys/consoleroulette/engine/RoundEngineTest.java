package com.leandronunes85.gamesys.consoleroulette.engine;

import com.google.common.collect.ImmutableMap;
import com.leandronunes85.gamesys.consoleroulette.model.User;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.Map;

/**
 * Created by nunesl on 01/12/15.
 */
public class RoundEngineTest {

    private static final double FACTOR = 5.0;

    private RoundEngine victim;
    private Map<String, User> users;

    @BeforeMethod
    public void setUp() throws Exception {
        users = ImmutableMap.of("User", new User("User"));

        victim = new RoundEngine(
                users,
                ImmutableMap.of("1", new OutcomeCalculatorImpl(1, FACTOR)));
    }

    @Test
    public void shouldCountBetsButNoWinningsWhenLosesBet() {
        victim.newLine("User 1 10.0"); // Bet on "1"
        victim.endRound(2); // "2" won

        User actual = users.get("User");

        Assert.assertEquals(actual.getTotalBet(), 10.0);
        Assert.assertEquals(actual.getTotalWin(), 0.0);
    }

    @Test
    public void shouldCountBetsAndWinningsWhenWinsBet() {
        victim.newLine("User 1 10.0"); // Bet on "1"
        victim.endRound(1); // "1" won

        User actual = users.get("User");

        Assert.assertEquals(actual.getTotalBet(), 10.0);
        Assert.assertEquals(actual.getTotalWin(), 10.0 * FACTOR);
    }
}