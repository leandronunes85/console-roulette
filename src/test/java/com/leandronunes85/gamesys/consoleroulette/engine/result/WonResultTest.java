package com.leandronunes85.gamesys.consoleroulette.engine.result;

import com.leandronunes85.gamesys.consoleroulette.model.Outcome;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

/**
 * Created by nunesl on 01/12/15.
 */
public class WonResultTest {

    private WonResult victim;

    @BeforeMethod
    public void setUp() {
        victim = new WonResult(10.0);
    }

    @Test
    public void shouldAlwaysReturnWonOutcome() {
        Outcome actual = victim.getOutcome();
        Assert.assertEquals(actual, Outcome.WON);
    }

    @Test
    public void shouldComputeWinningsCorrectly() {

        double actual = victim.computeWinnings(5.0);
        Assert.assertEquals(actual, 50.0);
    }
}