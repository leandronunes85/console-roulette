package com.leandronunes85.gamesys.consoleroulette.engine;

import com.leandronunes85.gamesys.consoleroulette.engine.result.Result;
import com.leandronunes85.gamesys.consoleroulette.model.Outcome;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.stream.IntStream;

/**
 * Created by nunesl on 01/12/15.
 */
public class OutcomeCalculatorImplTest {

    private static final double FACTOR = 10.0;

    private OutcomeCalculatorImpl victim;

    @BeforeMethod
    public void setUp() {
        victim = new OutcomeCalculatorImpl(IntStream.of(1, 2), FACTOR);
    }

    @Test
    public void shouldReturnWonForIncludedNumbers() {
        int drawn = 1;
        double stake = 10.0;

        Result actual = victim.computeResult(drawn);

        Assert.assertEquals(actual.getOutcome(), Outcome.WON);
        Assert.assertEquals(actual.computeWinnings(stake), stake * FACTOR);
    }

    @Test
    public void shouldReturnLostForNonIncludedNumbers() {
        int drawn = 3;
        double stake = 10.0;

        Result actual = victim.computeResult(drawn);

        Assert.assertEquals(actual.getOutcome(), Outcome.LOST);
        Assert.assertEquals(actual.computeWinnings(stake), 0.0);
    }
}