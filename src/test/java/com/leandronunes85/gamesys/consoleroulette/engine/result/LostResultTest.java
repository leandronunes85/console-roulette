package com.leandronunes85.gamesys.consoleroulette.engine.result;

import com.leandronunes85.gamesys.consoleroulette.model.Outcome;
import org.testng.Assert;
import org.testng.annotations.Test;

/**
 * Created by nunesl on 01/12/15.
 */
public class LostResultTest {

    private Result victim = LostResult.INSTANCE;

    @Test
    public void shouldAlwaysReturnNoWinnings() {
        double actual = victim.computeWinnings(100.0);
        Assert.assertEquals(actual, 0.0);
    }

    @Test
    public void shouldAlwaysReturnLostOutcome() {
        Outcome actual = victim.getOutcome();
        Assert.assertEquals(actual, Outcome.LOST);
    }
}